## 1. 요구사항

### 1) 관련 패키지 설치
- epel-release 설치

```bash
master1$ sudo yum -y install epel-release 
```

- python3 및 git 설치
```bash
master1$ sudo yum -y install python3 python3-pip git
```

### 2) 키 기반 인증
- kubespray에 사용되는 모든 노드에 ssh key 배포
```bash
master1$ ssh-keygen
```
```bash
master1$ for i in master1 master2 master3 node1 node2 node3  
> do  
> ssh-copy-id $i 
> done 
```

### 3) kubespray 다운로드
```bash
master1$ git clone https://github.com/kubernetes-sigs/kubespray.git
```

### 4) 요구사항 설치
```bash
master1$ cd kubespray
```
```bash
master1$ sudo pip3 install -r requirements.txt
```

### 5) 방화벽 서비스 중단
- 배포 도중 문제를 피하기위해 서비스 중단
```bash
master1$ cd ~
```
```bash
master1$ mkdir requirement && cd requirement
```

- ansible 설정 파일 작성
```bash
master1$ vi ansible.cfg  
[defaults]
inventory = inventory
host_key_checking = False      
```

- 인벤토리 작성
```bash
master1$ vi inventory
master1         ansible_host=10.0.0.11 
master2         ansible_host=10.0.0.12 
master3         ansible_host=10.0.0.13 
node1           ansible_host=10.0.0.21 
node2           ansible_host=10.0.0.22 
node3           ansible_host=10.0.0.23 
```

- 방화벽 중단 플레이북 작성
```bash
master1$ vi stop-fw.yaml  
- name: stop firewall service on the all node 
  hosts: all
  become: true
  gather_facts: no
  tasks:
  - name: stop and disable service
    systemd:
      name: firewalld
      state: stopped
      enabled: false
```

- 플레이북 실행
```bash
master1$ ansible-playbook stop-fw.yaml
```

## 2. kubespray 설치
### 1) 인벤토리 생성
```bash
master1$ cd ~/kubespray/ 
```

- 샘플파일 복사
```bash
master1$ cp -r inventory/sample/ inventory/mycluster 
```

- 인벤토리 작성
```bash
$ cat inventory/mycluster/inventory.ini
[all]
master1 ansible_host=10.0.0.11  ip=10.0.0.11  master2 ansible_host=10.0.0.12  ip=10.0.0.12  
master3 ansible_host=10.0.0.13  ip=10.0.0.13  
node1   ansible_host=10.0.0.21  ip=10.0.0.21  
node2   ansible_host=10.0.0.22  ip=10.0.0.22  node3   ansible_host=10.0.0.23  ip=10.0.0.23  
[kube-master]
master1
master2
master3

[etcd]
master1
master2
master3

[kube-node]
node1
node2
node3

[calico-rr]

[k8s-cluster:children]
kube-master
kube-node
calico-rr
```

### 2) 변수 설정

***************** 여기부터 다시 작성 **************
