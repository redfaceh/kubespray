## helm big concept

> Repository: Chart가 저장된 저장소
```bash
$ helm repo list
NAME  	URL                                              
stable	https://kubernetes-charts.storage.googleapis.com/
```

> Chart: 쿠버네티스에서 패키지와 같은 것. yum의 rpm
```bash
$ helm show chart stable/mysql 
apiVersion: v1
appVersion: 5.7.30
description: Fast, reliable, scalable, and easy to use open-source relational database
  system.
home: https://www.mysql.com/
icon: https://www.mysql.com/common/logos/logo-mysql-170x115.png
keywords:
- mysql
- database
- sql
maintainers:
- email: o.with@sportradar.com
  name: olemarkus
- email: viglesias@google.com
  name: viglesiasce
name: mysql
sources:
- https://github.com/kubernetes/charts
- https://github.com/docker-library/mysql
version: 1.6.6
```


> Release: 쿠버네티스에서 Chart의 단위. 동일한 쿠버네티스에서 다수의 Chart가 실행가능한데, 이 때 Relase로 분류함(Relase = Name)
```bash
$ helm ls
NAME            	NAMESPACE	REVISION	UPDATED                             	STATUS  	CHART      	APP VERSION
mysql-1594969954	default  	1       	2020-07-17 16:12:37.471777 +0900 KST	deployed	mysql-1.6.6	5.7.30     
mysql-1594969959	default  	1       	2020-07-17 16:12:41.975145 +0900 KST	deployed	mysql-1.6.6	5.7.30 
```

## helm search
> helm search hub: hub는 다수의 repo에 저장되어 있는 chart를 포함하고 있음
```bash
helm search hub mysql    
URL                                               	CHART VERSION	APP VERSION	DESCRIPTION                                       
https://hub.helm.sh/charts/cetic/adminer          	0.1.3        	4.7.6      	Adminer is a full-featured database management ...
https://hub.helm.sh/charts/appscode/stash-mysql   	8.0.14       	8.0.14     	stash-mysql - MySQL database backup and restore...
...
```

- https://hub.helm.sh/charts/cetic/adminer 에서 cetic이 repo를 나타냄

> helm search repo
```bash
$ helm repo ls 
NAME  	URL                                              
stable	https://kubernetes-charts.storage.googleapis.com/
```

## helm install
> helm install RELEASE REPO/CHART
```bash
$ helm install db1 stable/mysql        
NAME: db1
LAST DEPLOYED: Fri Jul 17 16:41:15 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
db1-mysql.default.svc.cluster.local

To get your root password run:

    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace default db1-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)

To connect to your database:

1. Run an Ubuntu pod that you can use as a client:

    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

2. Install the mysql client:

    $ apt-get update && apt-get install mysql-client -y

3. Connect using the mysql cli, then provide your password:
    $ mysql -h db1-mysql -p

To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
    kubectl port-forward svc/db1-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}

```

> helm ls
```bash
$ helm ls
NAME	NAMESPACE	REVISION	UPDATED                             	STATUS  	CHART      	APP VERSION
db1 	default  	1       	2020-07-17 16:41:15.530052 +0900 KST	deployed	mysql-1.6.6	5.7.30     
```

> helm status
```bash
$ helm status db1  
NAME: db1
LAST DEPLOYED: Fri Jul 17 16:41:15 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
db1-mysql.default.svc.cluster.local

To get your root password run:

    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace default db1-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)

To connect to your database:

1. Run an Ubuntu pod that you can use as a client:

    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

2. Install the mysql client:

    $ apt-get update && apt-get install mysql-client -y

3. Connect using the mysql cli, then provide your password:
    $ mysql -h db1-mysql -p

To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
    kubectl port-forward svc/db1-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}

```